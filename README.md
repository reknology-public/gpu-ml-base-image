# gpu-ml-base-image

## Base docker image aimed at ml applications

    This is a pre-compiled docker image you can import to a dockerfile to easily convert a gpu enabled machine learning program to a gpu docker container

## how to import:
    FROM reknology/public:gpu-ml-base-image 

    at the top of your dockerfile

## This image includes :
    Cuda 11
    Python3
    OpenCV 4.5.1
    Ubuntu 18.04
    ffmpeg
    numpy

## This makes it suitable for :
    Tensorflow (2.4.0)
    Pytorch 

    Other ML frameworks may be supported either fully or partialy 

Containers can be compiled on any version of docker. However they require nvidia-docker2 in order to be run with gpu acceleration

nvidia-docker2 instilation varies on host machine and os so you should follow the official installation instructions. It can be installed on most operating systems with the correct graphics cards, including WSL 2 for windows and most linux distrabutions

Running compiled images will look something like this : docker run --gpus all my_image:latest

## Caveats:
    wsl2 nvidia-docker instilation is different from otehr linux instilations and is a potential pitfall
    using OpenCV with python will require major version matching to the images version. ie opencv-python==4.5.1.48 works
    docker-compose doesnt support gpu acceleration by default, there are work arounds avalible.

## DockerHub link:

    https://hub.docker.com/r/reknology/public/tags


## Now get building!
You are now ready to get working on your newest machine learning project using cuda and openCV!

If you want some reference for making machine learning tools check out this book!
 
[Hands-on Machine Learning with Scikit-Learn, Keras, and TensorFlow: Concepts, Tools, and Techniques to Build Intelligent Systems by Aurelien Geron](https://www.amazon.co.uk/gp/product/1492032646/ref=as_li_tl?ie=UTF8&camp=1634&creative=6738&creativeASIN=1492032646&linkCode=as2&tag=jakenology-21&linkId=95ba027aad0b9d85e83f52eed0252de8) 
    
